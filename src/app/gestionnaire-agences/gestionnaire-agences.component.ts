import { Component, OnInit } from '@angular/core';

 interface  PeriodicElement {
   nom : string ;
  position : number ;
  poids : number ;
  symbole : string ;
}

const ELEMENT_DATA : PeriodicElement[] = [
  { position : 1 , nom : 'Hydrogène' , poids : 1.0079 , symbole : 'H' },
  { position : 2 , nom : 'Helium' , poids : 4.0026 , symbole : 'He' },
  { position : 3 , nom : 'Lithium' , poids : 6.941 , symbole : 'Li' },
  { position : 4 , nom : 'Béryllium' , poids : 9.0122 , symbole : 'Be' },
  { position : 5 , nom : 'Bore' , poids : 10.811 , symbole : 'B' },
  { position : 6 , nom : 'Carbon' , poids : 12.0107 , symbole : 'C' },
  { position : 7 , nom : 'Azote' , poids : 14.0067 , symbole : 'N' },
  { position : 8 , nom : 'Oxygen' , poids : 15.9994 , symbole : 'O' },
  { position : 9 , nom : 'Fluor' , poids : 18.9984 , symbole : 'F' },
  { position : 10 , nom : 'Neon' , poids : 20.1797 , symbole : 'Ne' },
] ;

@Component({
  selector: 'app-gestionnaire-agences',
  templateUrl: './gestionnaire-agences.component.html',
  styleUrls: ['./gestionnaire-agences.component.css']
})
export class GestionnaireAgencesComponent  {
  element ={ position:'', name:'' , poids:'' , symbole:'' };
  dataSource = ELEMENT_DATA;
  clickedRows = new  Set <PeriodicElement>();

  constructor() { }

  ngOnInit(): void {
  }



}
