import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionnaireAgencesComponent } from './gestionnaire-agences.component';

describe('GestionnaireAgencesComponent', () => {
  let component: GestionnaireAgencesComponent;
  let fixture: ComponentFixture<GestionnaireAgencesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestionnaireAgencesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionnaireAgencesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
