import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanificatuerVoyagesComponent } from './planificatuer-voyages.component';

describe('PlanificatuerVoyagesComponent', () => {
  let component: PlanificatuerVoyagesComponent;
  let fixture: ComponentFixture<PlanificatuerVoyagesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlanificatuerVoyagesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanificatuerVoyagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
