import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionnaireAgenceComponent } from './gestionnaire-agence.component';

describe('GestionnaireAgenceComponent', () => {
  let component: GestionnaireAgenceComponent;
  let fixture: ComponentFixture<GestionnaireAgenceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestionnaireAgenceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionnaireAgenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
