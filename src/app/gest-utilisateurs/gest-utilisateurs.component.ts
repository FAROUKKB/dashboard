import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-gest-utilisateurs',
  templateUrl: './gest-utilisateurs.component.html',
  styleUrls: ['./gest-utilisateurs.component.css']
})
export class GestUtilisateursComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }
  redirect() {
    this.router.navigate(['./ajouter']);
  }
}
