import { Component, OnInit,Inject } from '@angular/core';
import {Router} from '@angular/router';

export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: 'app-gest-agences',
  templateUrl: './gest-agences.component.html',
  styleUrls: ['./gest-agences.component.css'],
})
export class GestAgencesComponent implements OnInit {


  constructor(private router: Router) { }

  ngOnInit(): void {
    //listagence=axios.get('/agences')
  }

  redirect() {
    this.router.navigate(['./ajouter']);
  }
  deleteAgence(id:number):void{
    this.agencesList=this.agencesList.filter((item)=>item.id!=id)
   // axios.delete('/',id)
   //listagence=axios.get('/agences')
    console.log(this.agencesList)
  }
  changeStatus(id:number):void{
    this.agencesList.map(item=>{
      if(item.id==id){
        item.selected=!item.selected
      }
    })
  }
   agencesList :agenceInterface[] =[
    {
      agence:"sentri",
      id:1,
      nmbre_voy:25,
      time:"il y'a 5 min",
      selected:false
    },
    {
      agence:"sentri 1",
      id:2,
      nmbre_voy:25,
      time:"il y'a 5 min",
      selected:false
    },
    {
      agence:"sentri 2",
      id:3,
      nmbre_voy:25,
      time:"il y'a 5 min",
      selected:false
    },
    {
      agence:"sentri 3",
      id:4,
      nmbre_voy:25,
      time:"il y'a 5 min",
      selected:false
    },
    {
      agence:"sentri 4",
      id:5,
      nmbre_voy:25,
      time:"il y'a 5 min",
      selected:false
    },
    {
      agence:"sentri 5",
      id:6,
      nmbre_voy:25,
      time:"il y'a 5 min",
      selected:false
    },
    {
      agence:"sentri 6",
      id:7,
      nmbre_voy:25,
      time:"il y'a 5 min",
      selected:false
    },
    {
      agence:"sentri 7",
      id:8,
      nmbre_voy:25,
      time:"il y'a 5 min",
      selected:false
    },
  ]
}
interface agenceInterface {
  agence:string,
  id:number,
  nmbre_voy:number,
  time:string,
  selected:boolean
}
