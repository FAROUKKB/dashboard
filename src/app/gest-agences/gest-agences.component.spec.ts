import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestAgencesComponent } from './gest-agences.component';

describe('GestAgencesComponent', () => {
  let component: GestAgencesComponent;
  let fixture: ComponentFixture<GestAgencesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestAgencesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GestAgencesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
