import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GestionnaireAgenceComponent } from './gestionnaire-agence/gestionnaire-agence.component';
import { GestionnaireAgencesComponent } from './gestionnaire-agences/gestionnaire-agences.component';
import { AgencesComponent } from './agences/agences.component';
import { GestAgencesComponent } from './gest-agences/gest-agences.component';
import { GestUtilisateursComponent } from './gest-utilisateurs/gest-utilisateurs.component';
import { DialogFormComponent } from './dialog-form/dialog-form.component';
import { PlanificatuerVoyagesComponent } from './planificatuer-voyages/planificatuer-voyages.component';
import { HistoriqueTicketsComponent } from './historique-tickets/historique-tickets.component';

@NgModule({
  declarations: [
    AppComponent,
    GestionnaireAgenceComponent,
    GestionnaireAgencesComponent,
    AgencesComponent,
    GestAgencesComponent,
    GestUtilisateursComponent,
    DialogFormComponent,
    PlanificatuerVoyagesComponent,
    HistoriqueTicketsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
