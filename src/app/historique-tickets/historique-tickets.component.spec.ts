import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoriqueTicketsComponent } from './historique-tickets.component';

describe('HistoriqueTicketsComponent', () => {
  let component: HistoriqueTicketsComponent;
  let fixture: ComponentFixture<HistoriqueTicketsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HistoriqueTicketsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoriqueTicketsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
