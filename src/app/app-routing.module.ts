import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AgencesComponent } from './agences/agences.component';
import { DialogFormComponent } from './dialog-form/dialog-form.component';
import { GestionnaireAgenceComponent } from './gestionnaire-agence/gestionnaire-agence.component';
import { GestionnaireAgencesComponent } from './gestionnaire-agences/gestionnaire-agences.component';
import { PlanificatuerVoyagesComponent } from './planificatuer-voyages/planificatuer-voyages.component';

const routes: Routes = [
  { path: 'agence', component: GestionnaireAgenceComponent },
  { path: 'agences', component:GestionnaireAgencesComponent},
  { path: '', component:AgencesComponent},
  {path: 'ajouter', component:DialogFormComponent},
  {path:'farouk', component:AgencesComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

 }
